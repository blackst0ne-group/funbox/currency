class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def self._arel
    name.demodulize.constantize.arel_table
  end
end
