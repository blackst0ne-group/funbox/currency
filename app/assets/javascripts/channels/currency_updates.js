App.currency_updates = App.cable.subscriptions.create("CurrencyUpdatesChannel", {
  connected: function() {
    // Called when the subscription is ready for use on the server
  },

  disconnected: function() {
    // Called when the subscription has been terminated by the server
  },

  received: function (data) {
    console.log(data)
    console.log('---')
    const page = new Page(data)

    page.show()
  }
});
