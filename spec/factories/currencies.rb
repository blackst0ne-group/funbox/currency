FactoryBot.define do
  factory :currency do
    code        "USD"
    nominal     "1"
    name        "Доллар США"
    source      "Cbr"
    value       "30"
    valid_until Time.zone.now + 10.minutes
  end

  trait :custom_source do
    source Currency::CUSTOM_SOURCE
  end
end
