# Scraper for cbr.ru
module Scrapers
  class Cbr < Scrapers::Base
    def initialize(currency = "USD")
      @currency = currency
      @source = source
    end

    def scrap
      parse(load_source)
    end

    private

    def parse(source)
      doc = Nokogiri::XML(source, nil, "windows-1251")
      doc = doc.xpath("//ValCurs/Valute[CharCode = '#{@currency}']")

      Currency.new(
        doc.children[1].text, # code
        doc.children[2].text, # nominal
        doc.children[3].text, # name
        doc.children[4].text, # value
        source_name,
        Date.today.end_of_day
      )
    end

    def source(date = Date.today)
      date = date.to_s(:slashed)

      "http://www.cbr.ru/scripts/XML_daily.asp?date_req=#{date}"
    end
  end
end
