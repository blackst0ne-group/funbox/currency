require "rails_helper"

RSpec.describe PagesController do
  set(:currency) { create(:currency) }

  describe "GET #index" do
    it "returns http success" do
      get :index

      expect(response).to be_successful
      expect(response).to render_template(:index)
    end
  end
end
