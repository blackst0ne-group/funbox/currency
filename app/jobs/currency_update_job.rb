class CurrencyUpdateJob < ApplicationJob
  queue_as :currency

  def perform(*args)
    ScraperService.new(Scrapers::Cbr.new).call
  end
end
