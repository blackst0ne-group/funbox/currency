class Page {
  constructor(data) {
    this.code        = data.code
    this.name        = data.name
    this.nominal     = data.nominal
    this.source      = data.source
    this.valid_until = data.valid_until
    this.value       = data.value
  }

  show() {
    document.getElementById("code").innerHTML        = this.code
    document.getElementById("name").innerHTML        = this.name
    document.getElementById("nominal").innerHTML     = this.nominal
    document.getElementById("source").innerHTML      = this.source
    document.getElementById("valid_until").innerHTML = this.valid_until
    document.getElementById("value").innerHTML       = this.value
  }
}
