require "rails_helper"

RSpec.describe Scrapers::Base do
  describe "#load_source" do
    it "loads data" do
      allow(HTTParty).to receive_message_chain(:get, :body) { "loaded data" }

      data = described_class.new.__send__(:load_source)

      expect(data).to eq("loaded data")
    end
  end

  describe "#source_name" do
    it "returns class name without ancestors" do
      source_name = described_class.new.__send__(:source_name)

      expect(source_name).to eq("Base")
    end
  end

  %w(parse scrap source).each do |_method|
    describe "##{_method}" do
      it "throws exception" do
        error_message = /`#{_method}` method must be implemented in/

        expect{ described_class.new.__send__(_method) }.to raise_error(NotImplementedError, error_message)
      end
    end
  end
end
