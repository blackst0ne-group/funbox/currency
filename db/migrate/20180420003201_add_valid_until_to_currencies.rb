class AddValidUntilToCurrencies < ActiveRecord::Migration[5.2]
  def change
    add_column :currencies, :valid_until, :datetime
  end
end
