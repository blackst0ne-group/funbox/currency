require "rails_helper"

RSpec.describe Scrapers::Cbr do
  let(:data) { file_fixture("cbr.xml").read }

  describe "#scrap" do
    it "scraps data" do
      allow(HTTParty).to receive_message_chain(:get, :body) { data }

      scraped_data = described_class.new.scrap

      expect(scraped_data).to be_a(Struct)
      expect(scraped_data.code).to eq("USD")
      expect(scraped_data.nominal).to eq("1")
      expect(scraped_data.name).to eq("Доллар США")
      expect(scraped_data.value).to eq("63,2396")
      expect(scraped_data.source).to eq("Cbr")
      expect(scraped_data.valid_until).to eq(Date.today.end_of_day)
    end
  end

  %w(scrap source).each do |_method|
    describe "##{_method}" do
      it "throws exception" do
        error_message = /`#{_method}` method must be implemented in/

        expect{ described_class.new.__send__(_method) }.not_to raise_error
      end
    end
  end
end
