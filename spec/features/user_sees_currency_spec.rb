require "rails_helper"

RSpec.describe "User sees currency" do
  set(:currency) { create(:currency) }

  before do
    visit(root_path)
  end

  it "shows currency" do
    expect(page).to have_content("Актуально на")
               .and have_content("Валюта")
               .and have_content("Источник")
               .and have_content("Код")
               .and have_content("Курс")
               .and have_content("Номинал")
               .and have_content(currency[:code])
               .and have_content(currency[:name])
               .and have_content(currency[:nominal])
               .and have_content(currency[:source])
               .and have_content(currency[:valid_until].to_s(:dotted))
               .and have_content(currency[:value])
  end

  context "when adding new currency" do
    it "refreshes data in all windows", :js do
      expect(page).to have_content(currency[:valid_until].to_s(:dotted))
                 .and have_content(currency[:value])

      new_window = open_new_window

      # Check values in the second window.
      within_window(new_window) do
        visit(root_path)

        expect(page).to have_content(currency[:source])
                   .and have_content(currency[:valid_until].to_s(:dotted))
                   .and have_content(currency[:value])
      end

      # Create new custom currency and check if the root page gets updated in both windows.
      custom_currency = create(:currency, :custom_source, value: 100, valid_until: Time.zone.now + 1.day)

      # Check data update in the first window.
      expect(page).to have_content(custom_currency[:source])
                 .and have_content(custom_currency[:valid_until].to_s(:dotted))
                 .and have_content(custom_currency[:value])
                 .and have_no_content(currency[:source])
                 .and have_no_content(currency[:valid_until].to_s(:dotted))
                 .and have_no_content(currency[:value])

      # Check data update in the second window.
      within_window(new_window) do
        expect(page).to have_content(custom_currency[:source])
                  .and have_content(custom_currency[:valid_until].to_s(:dotted))
                  .and have_content(custom_currency[:value])
                  .and have_no_content(currency[:source])
                  .and have_no_content(currency[:valid_until].to_s(:dotted))
                  .and have_no_content(currency[:value])
      end
    end
  end
end
