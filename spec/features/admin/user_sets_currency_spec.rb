require "rails_helper"

RSpec.describe "User sets currency" do
  context "when there is no custom currency" do
    before do
      visit(admin_path)
    end

    it "shows layout" do
      expect(page).to have_content("Курс доллара США")
                 .and have_content("Показывать до")
                 .and have_button("Сохранить")
    end

    it "shows no currency" do
      expect(find_field("currency").text).to be_blank
      expect(find_field("valid-until").text).to be_blank
    end

    context "when adding new currency" do
      context "when data is invalid" do
        context "when value is invalid" do
          it "shows validation error" do
            fill_in("valid-until", with: Time.zone.now + 10.minutes)
            click_button("Сохранить")

            expect(page).to have_content(/Произошло ужасное! А именно:/)
          end
        end

        context "when valid_until is invalid" do
          it "shows validation error" do
            fill_in("currency", with: 10)
            click_button("Сохранить")

            expect(page).to have_content(/Произошло ужасное! А именно:/)
          end
        end
      end

      context "when data is valid" do
        it "saves currency" do
          currency = 10
          valid_until = Time.zone.now + 10.minutes

          fill_in("currency", with: currency)
          fill_in("valid-until", with: valid_until)
          click_button("Сохранить")

          expect(page).to have_content("Всё огонь!")

          visit(root_path)

          expect(page).to have_content(currency)
                     .and have_content(valid_until.to_s(:dotted))
        end
      end
    end
  end

  context "when there is a custom currency" do
    set(:custom_currency) { create(:currency, :custom_source) }

    before do
      visit(admin_path)
    end

    it "shows currency" do
      expect(page).to have_field("currency", with: custom_currency.value)
      expect(page).to have_field("valid-until", with: custom_currency.valid_until.to_s(:no_timezone))
      expect(page).not_to have_css("div.flatpickr-calendar.open")
    end

    it "shows picker", :js do
      find_field("valid-until").click

      expect(page).to have_css("div.flatpickr-calendar.open")
    end
  end
end
