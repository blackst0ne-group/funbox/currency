source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.1'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.0'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

# Twitter Bootstrap
gem 'bootstrap', '~> 4.1.0'
gem 'jquery-rails'

# JSON.
gem 'jbuilder'

# HTTP.
gem 'httparty'

# Parser.
gem 'nokogiri'

# Application server.
gem 'puma', '~> 3.11'

#Caching.
gem 'redis'

# Background jobs.
gem 'sidekiq'

# Database.
gem 'sqlite3'

# JavaScript.
gem 'therubyracer'

# Cron jobs.
gem 'whenever', require: false

group :development do
  gem 'foreman'
  gem 'rubocop', require: false
  gem 'listen', '>= 3.0.5', '< 3.2'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  gem 'capybara'
  gem 'capybara-screenshot'
  gem 'capybara-selenium'
  gem 'database_cleaner'
  gem 'rails-controller-testing'
  gem 'rspec-set'
  gem 'shoulda-matchers'
end

group :development, :test do
  gem 'factory_bot_rails'
  gem 'pry-byebug', platform: :mri
  gem 'pry-rails'
  gem 'rspec-rails'
end
