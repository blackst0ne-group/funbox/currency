class Currency < ApplicationRecord
  CACHE_KEY = "currency_current".freeze
  CUSTOM_SOURCE = "custom".freeze

  after_commit :update_cache
  after_commit :send_to_currency_updates_channel

  validates :code, :name, :nominal, :source, :value, presence: true
  validate :valid_until_datetime

  scope :up_to_date,            -> { where(_arel[:valid_until].gteq(Time.zone.now)) }
  scope :without_custom_source, -> { where.not(source: CUSTOM_SOURCE) }
  scope :with_custom_source,    -> { where(source: CUSTOM_SOURCE) }

  def send_to_currency_updates_channel
    ActionCable.server&.broadcast(CurrencyUpdatesChannel::CHANNEL_NAME, self.class.cacheable_record(self))
  end

  def update_cache
    Rails.cache.write(CACHE_KEY, self.class.cacheable_record(self), expires_in: cache_expiration_time)

    # Send the last non-custom actual currency to clients just once from backend.
    SendCurrencyToChannelJob.set(wait_until: valid_until).perform_later
  end

  class << self
    def cacheable_record(record)
      {
        code:        record.code,
        nominal:     record.nominal,
        name:        record.name,
        value:       record.value,
        source:      record.source,
        valid_until: record.valid_until.to_s(:dotted)
      }
    end

    def current
      Rails.cache.fetch(CACHE_KEY) { cacheable_record(current_currency) }
    end

    def current_currency
      with_custom_source.up_to_date.or(without_custom_source.up_to_date).last
    end

    def up_to_date?
      up_to_date.exists?
    end
  end

  private

  # In seconds.
  def cache_expiration_time
    @expires_in ||= (valid_until - Time.zone.now).to_i
  end

  def valid_until_datetime
    valid_until.to_datetime rescue errors.add(:valid_until, "must be a datetime")
  end
end
