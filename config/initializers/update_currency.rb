# Run scrapper on application start if there's no any up-to-date record exists.
 CurrencyUpdateJob.perform_now if !Rails.env.test? &&
                                  ActiveRecord::Base.connection.table_exists?("Currency") &&
                                  !Currency.up_to_date?
