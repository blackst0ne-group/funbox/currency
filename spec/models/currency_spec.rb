require "rails_helper"

RSpec.describe Currency do
  context "when adding currencies" do
    let(:currency) { build(:currency) }

    describe "after_commits" do
      it "calls #send_to_currency_updates_channel" do
        expect(currency).to receive(:send_to_currency_updates_channel)

        currency.save!
      end

      it "calls #update_cache" do
        expect(currency).to receive(:update_cache)

        currency.save!
      end
    end

    describe "validations" do
      it { is_expected.to validate_presence_of(:code) }
      it { is_expected.to validate_presence_of(:name) }
      it { is_expected.to validate_presence_of(:nominal) }
      it { is_expected.to validate_presence_of(:source) }
      it { is_expected.to validate_presence_of(:value) }

      describe "#valid_until_datetime" do
        before do
          currency.valid?
        end

        context "when :valid_until is valid" do
          it "doesnt't return validation error" do
            expect(currency.errors.size).to eq(0)
          end
        end

        context "when :valid_until is invalid" do
          let(:currency) { build(:currency, valid_until: "invalid_value") }

          it "returns validation error" do
            expect(currency.errors[:valid_until].first).to eq("must be a datetime")
          end
        end
      end
    end

    describe "#cache_expiration_time" do
      it "returns correct value" do
        expires_in = currency.__send__(:cache_expiration_time)
        seconds = (currency.valid_until - Time.zone.now).to_i

        expect(expires_in).to eq(seconds)
      end
    end

    describe "#send_to_currency_updates_channel" do
      it "sends updates to channels" do
        channel_name = CurrencyUpdatesChannel::CHANNEL_NAME
        record = described_class.cacheable_record(currency)

        expect(ActionCable.server).to receive(:broadcast).with(channel_name, record)

        currency.send_to_currency_updates_channel
      end
    end

    describe "#update_cache" do
      it "updates the cache" do
        expect(Rails.cache).to receive(:write).with(described_class::CACHE_KEY, anything, anything)

        currency.update_cache
      end

      it "creates a job" do
        expect(SendCurrencyToChannelJob).to receive_message_chain(:set, :perform_later)

        currency.update_cache
      end
    end
  end

  context "when currencies exist" do
    set(:currency_1) { create(:currency, value: 50, valid_until: Time.zone.now + 5.minutes, source: "custom") }
    set(:currency_2) { create(:currency, value: 60, valid_until: Time.zone.now + 20.minutes) }
    set(:currency_3) { create(:currency, value: 20, valid_until: Time.zone.now + 2.minutes) }
    set(:currency_4) { create(:currency, value: 70, valid_until: Time.zone.now + 10.minutes, source: "custom") }
    set(:currency_5) { create(:currency, value: 90, valid_until: Time.zone.now - 30.minutes) }

    describe ".current" do
      it "returns correct record" do
        current = described_class.current

        expect(current.class).to eq(Hash)
        expect(current[:valid_until]).to eq(currency_4.valid_until.to_s(:dotted))
        expect(current[:source]).to eq(currency_4.source)
        expect(current[:value]).to eq(currency_4.value)
      end
    end

    describe "scopes" do
      describe "current_currency" do
        it "returns record" do
          currency = described_class.current_currency

          expect(currency.id).to eq(currency_4.id)
        end
      end

      describe "up_to_date" do
        it "returns records" do
          currencies = described_class.up_to_date

          expect(currencies.size).to eq(4)
          expect(currencies).not_to include(currency_5)
        end
      end

      describe "without_custom_source" do
        it "returns records" do
          currencies = described_class.without_custom_source

          expect(currencies.size).to eq(3)
        end
      end

      describe "with_custom_source" do
        it "returns records" do
          currencies = described_class.with_custom_source

          expect(currencies.size).to eq(2)
        end
      end
    end

    describe ".cacheable_record" do
      it "returns sanitized hash" do
        cacheable_record = described_class.cacheable_record(currency_1)

        expect(cacheable_record.class).to eq(Hash)
        expect(cacheable_record.keys.size).to eq(6)
        expect(cacheable_record.keys).to include(:code, :nominal, :name, :value, :source, :valid_until)
      end
    end

    describe ".up_to_date?" do
      it "returns currencies status" do
        currency = described_class.up_to_date?

        expect(currency).to be(true)
      end
    end
  end
end
