document.addEventListener('DOMContentLoaded', () => {
  const inputField = document.querySelector('.flatpickr')

  if (inputField) {
    flatpickr(inputField, {
      defaultDate: inputField.defaultValue,
      minDate: new Date(),
      mode: 'single',
      enableTime: true,
      time_24hr: true
    })
  }
})
