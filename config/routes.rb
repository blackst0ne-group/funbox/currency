Rails.application.routes.draw do
  namespace :admin do
    get "/",  to: "pages#index"
    post "/", to: "pages#create"
  end

  root "pages#index"
end
