require "rails_helper"

RSpec.describe ScraperService do
  let(:scraper) { Scrapers::Cbr }

  describe "#call" do
    it "scraps data and saves it" do
      allow_any_instance_of(scraper).to receive(:scrap).and_return({})
      allow_any_instance_of(Currency).to receive(:save).and_return(true)

      expect_any_instance_of(scraper).to receive(:scrap)
      expect_any_instance_of(Currency).to receive(:save)

      described_class.new(scraper.new).call
    end
  end

  describe "#save" do
    context "when data is valid" do
      let(:data) do
        {
          code: "USD",
          nominal: 1,
          name: "Доллар США",
          value: 30,
          source: "Cbr",
          valid_until: "2020-05-30 00:00:00"
        }
      end

      it "saves data" do
        allow(Rails.logger).to receive(:info)

        expect(Rails.logger).to receive(:info).with("Currency has been added")
        expect(Rails.logger).not_to receive(:error).with("Error on adding currency")

        described_class.new(scraper.new).save(data)
      end
    end

    context "when data is not valid" do
      let(:data) { { code: "USD" } }

      it "doesn't save data" do
        expect(Rails.logger).to receive(:error).with(/Error on adding currency/)
        expect(Rails.logger).not_to receive(:info).with("Currency has been added")

        described_class.new(scraper.new).save(data)
      end
    end
  end
end
