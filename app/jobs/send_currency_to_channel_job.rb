class SendCurrencyToChannelJob < ApplicationJob
  queue_as :channels

  def perform(*args)
    currency = Currency.without_custom_source.up_to_date.last

    ActionCable.server.broadcast(CurrencyUpdatesChannel::CHANNEL_NAME, currency)
  end
end
