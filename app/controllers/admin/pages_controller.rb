class Admin::PagesController < ApplicationController
  def index
    @currency = Currency.with_custom_source.last
    @value = @currency&.valid_until&.to_s(:no_timezone)
  end

  def create
    @currency = Currency.new(default_params.merge(currency_params))

    if @currency.save
      flash[:notice] = "Всё огонь!"

      redirect_to admin_path
    else
      flash[:alert] = "Произошло ужасное! А именно: #{currency_validation_errors}"

      render :index
    end
  end

  private

  def currency_params
    params.require(:currency).permit(:valid_until, :value)
  end

  def currency_validation_errors
    @currency.errors.full_messages.join(", ")
  end

  def default_params
    {
      code:    "USD",
      name:    "Доллар США",
      nominal: "1",
      source:  Currency::CUSTOM_SOURCE
    }
  end
end
