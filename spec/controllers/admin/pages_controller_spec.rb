require "rails_helper"

RSpec.describe Admin::PagesController do

  describe "GET #index" do
    it "returns http success" do
      get :index

      expect(response).to be_successful
      expect(response).to render_template(:index)
    end
  end

end
