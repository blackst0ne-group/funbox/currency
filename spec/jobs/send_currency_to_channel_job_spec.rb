require "rails_helper"

RSpec.describe SendCurrencyToChannelJob do
  include ActiveJob::TestHelper

  let(:channel_name) { CurrencyUpdatesChannel::CHANNEL_NAME }
  set(:currency) { Currency.new }

  describe "#perform_now" do
    it "broadcasts currency" do
      allow(ActionCable.server).to receive(:broadcast).with(channel_name, currency).and_return(true)

      expect(ActionCable.server).to receive(:broadcast)

      described_class.perform_now
    end
  end

  describe "#perform_later" do
    it "enqueues a broadcast call" do
      allow(ActionCable.server).to receive(:broadcast).with(channel_name, currency).and_return(true)

      described_class.perform_later

      expect(described_class).to have_been_enqueued.on_queue("channels")
    end
  end
end
