Date::DATE_FORMATS[:slashed] = "%d/%m/%Y"

Time::DATE_FORMATS[:dotted] = "%d.%m.%Y %H:%M:%S"
Time::DATE_FORMATS[:no_timezone] = "%Y-%m-%d %H:%M:%S"
