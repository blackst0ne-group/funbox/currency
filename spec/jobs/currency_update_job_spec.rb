require "rails_helper"

RSpec.describe CurrencyUpdateJob do
  include ActiveJob::TestHelper

  describe "#perform_now" do
    it "calls a scraper" do
      allow_any_instance_of(ScraperService).to receive(:call).and_return(true)

      expect_any_instance_of(ScraperService).to receive(:call)

      described_class.perform_now
    end
  end

  describe "#perform_later" do
    it "enqueues a scraper call" do
      described_class.perform_later

      expect(described_class).to have_been_enqueued.on_queue("currency")
    end
  end
end
