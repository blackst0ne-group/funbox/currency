# README

Тестовое web-приложение. На Rails.

По условиям задания плюсом было бы использование какого-нибудь JS-фреймворка.      
Но даже для такой тестовой задачи это было бы полный оверкиллом.     

На мои навыки работы с JS-фреймворком `VueJS` можно посмотреть на реальном проекте: [eqalert.ru](https://github.com/geophystech/eqalert.ru)

## Как установить приложение

Эти шаги нужно выполнить один раз перед первым запуском.

```bash
# Устанавливаем зависимости:
sudo apt-get install build-essential patch ruby-dev zlib1g-dev liblzma-dev sqlite3 

# Для feature-тестов требуется chrome или chromium. 
# В зависимости от дистрибутива, пакет может называть по-разному. 
sudo apt-get install chromium-chromedriver # или chromedriver.

bundle install

# Создаём sqlite-базы:
bin/rails db:create db:migrate db:test:prepare

# Если нужно автоматическое обновление данных:
whenever --update-crontab
```

## Как запустить приложение

Для запуска приложения используется `foreman`.

```bash
# Запускаем приложение:
foreman start
```

## Как запустить тесты

Код покрыт тестами. Для тестов используeтся `rspec`.

```bash
# Запустить все тесты:
rspec .
```

Если интересно посмотреть, что происходит при прогоне feature-тестов:

```bash
# Запустить feature-тесты с открытием браузера:
CHROME_HEADLESS=0 rspec spec/features
```
