class CurrencyUpdatesChannel < ApplicationCable::Channel
  CHANNEL_NAME = "currency_updates_channel".freeze

  def subscribed
    stream_from CHANNEL_NAME
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
