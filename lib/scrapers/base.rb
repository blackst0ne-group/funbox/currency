module Scrapers
  class Base
    Currency = Struct.new(:code, :nominal, :name, :value, :source, :valid_until)

    def scrap
      raise NotImplementedError, "`#{__method__}` method must be implemented in `#{self.class}`"
    end

    private

    def load_source
      HTTParty.get(@source).body
    end

    def source_name
      self.class.name.demodulize
    end

    %w(parse source).each do |_method|
      define_method(_method) do
        raise NotImplementedError, "`#{__method__}` method must be implemented in `#{self.class}`"
      end
    end
  end
end
