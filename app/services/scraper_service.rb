# This service scraps data from remote data sources using specific scaper.
class ScraperService
  attr_reader :scraper

  def initialize(scraper)
    @scraper = scraper
  end

  def call
    data = scraper.scrap

    save(data)
  end

  def save(data)
    currency = Currency.new(data.to_h)

    if currency.save
      Rails.logger.info("Currency has been added")
    else
      Rails.logger.error("Error on adding currency: #{currency.errors.first}")

      false
    end
  end
end
